/*My own codes:

import { Link } from "react-router-dom";

export default function ErrorPage() {
	return (
		<div>
			<h1>Page Not Found</h1>
			<Link to='/'>Go back to the homepage</Link> 
		</div>
	)
};
*/

import Banner from '../components/Banner';

export default function ErrorPage() {

	const data = {
		title: "Error 404 - Page not found.",
		content: "The page you are looking for cannot be found",
		destination: "/",
		label: "Back to Home"
	}

	return (
		<Banner data={data} />
	)
}